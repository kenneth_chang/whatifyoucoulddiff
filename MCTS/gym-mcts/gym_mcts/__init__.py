from gym.envs.registration import register

register(
    id='mcts-trivial-v0',
    entry_point='gym_mcts.envs:MCTS_Trivial',
)