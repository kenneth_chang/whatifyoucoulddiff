import gym
from gym import error, spaces, utils
from gym.utils import seeding


class Node():
  node_count:int = 0
  nodes:list = []

  def __init__(self, data:int, isLeaf:bool=False):
    self.state_index:int = Node.node_count
    Node.nodes.append(self)

    Node.node_count += 1 

    self.data:int = data
    self.left:Node = None
    self.right:Node = None
    self.isLeaf = isLeaf
    print(self)

  def __repr__(self):
    return f"The {self.state_index}th Node with value {self.data}."

#               0
#         3          1 
#     -5     5  -10     10
#
class MCTS_Trivial(gym.Env):
  metadata = {'render.modes': ['human']}

  def __init__(self):
    self.action_space = spaces.Box(0,1,(1,), dtype=int)
    self.observation_space = spaces.Discrete(7)

    
    self.current_node = self.root_node = self.create_tree() 
    self.state = self.current_node.state_index

  def create_tree(self) -> Node:
    n0 = Node(0)
    n1 = Node(3)
    n2 = Node(1)  
    n3 = Node(-5, isLeaf=True)  
    n4 = Node(5, isLeaf=True)  
    n5 = Node(-10, isLeaf=True)  
    n6 = Node(10, isLeaf=True)  

    n0.left  = n1
    n0.right = n2

    n1.left  = n3
    n1.right = n4

    n2.left  = n5
    n2.right = n6
    return n0

  def step(self, action):
    if action[0] == 0:
      self.current_node = self.current_node.left
      print("action 0")
    elif action[0] == 1:
      self.current_node = self.current_node.right
      print("action 1")
    else:
      exit(f"This sholdn't happen, action is: {action}")
    
    reward = self.current_node.data
    done = self.current_node.isLeaf
    self.state = self.current_node.state_index
    return [self.state, reward, done, None]
    
  def reset(self):
    self.current_node = self.root_node
    self.state = self.root_node.state_index

  def render(self, mode='human', close=False):
    print(f"In state {self.current_node.state_index} with value {self.current_node.data}")
    pass