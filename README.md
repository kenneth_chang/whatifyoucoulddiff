# WhatIfYouCouldDiff
Github repo for the second season of the Ken &amp; Batu Show

# Installation
First of try activating the retro_venv virtual environment. by calling source retro_venv/bin/activate.
Check if this has worked by running the following two commands in the Retro_Workshop folder.

python human_playthrough.py -m TestMovie -f 100 
python experiment.py -m TestMovie -a rrt

If you see any integration problems, follow the errors and use the scripts that are in the Integration folder. Even after running the intergration if there are errors consider installing your own virtual env.

To do this create a new virtual environment in the parent directory of Retro_Workshop.
Activate it.
run:
pip install -r requirements.txt
and then go into
<your_path>/<your_venv_name>/lib/python3.6/site-packages/retro

into that folder copy the
retro_env.py and data file and folders that can be found in the Integration/Library_Setup

If this errors reach out to Ken or Batu.

#Running test_gameplay.py
This script is meant to allow for ease of running test cases as well as have a built timer that will automatically close and
save all of the gameplay you record using retro. To run the scipt type: "python3 test_gameplay" -- from here you will be prompted
with various arguments to be inserted -- type according to what you are prompted to type. The script will probably not work properly
at first. To fix this open the test_gameplay.py file and change the following variables: modifieid_rom_path, rom_location_path, and
directory. For the modified_rom_path place the directory's path that contains the modified roms you woudl want to be copying from. 
For the rom_location_path, copy the path for where you will want to copy the roms into. Finally, copy the directory path in which,
you are running human_playthrough.py out of into the directory variable. Any issuses or errors with the script message Nikhil.

# How to fix os.typeerror expected str,bytes,os.pathline not nonetype

Go to the retro_workplace/retro_integration/library_setup

Look inside retro_env.py, make sure this file exists

Copy this file to retro_venv/lib/python3.6/site-packages, replace the old one

Go to retro_workplace/integration/library_setup

Copy the data directory to retro_venv/lib/python3.6/site-packages/retro, merge the data directories.
