import os, matplotlib, pickle
import numpy as np

import matplotlib.pyplot as plt



# with open("Data/log_data.pickle", "rb") as f:
with open("Data/log_data_16_u2.pickle", "rb") as f:
    log_dict = pickle.load(f)


print(list(log_dict["x_y_level"])[0])
# print(len(log_dict["x_y_level"]))
# print(log_dict.keys())

# exit()

x_values = []
y_values = []
output_end = 7
output_start = 0

input_start = 5
input_end = 12
# x_y_level.add((_info["dungeonindex"],_info["worldindex"], _info["xPosLink"]//32, _info["yPosLink"]//32))
for map_x_y in log_dict["x_y_level"]:
    if (map_x_y[0] == 113 or map_x_y[0] == 114 or map_x_y[0] == 129 or map_x_y[0] == 130 ):
        #output = output_start + ((output_end - output_start) / (input_end - input_start)) * (map_x_y[2] - input_start)
        if(map_x_y[2]> 15 and map_x_y[3] > 100):
            x_values.append(map_x_y[2])
            y_values.append(map_x_y[3])

print(str(len(x_values)))
print(str(len(y_values)))
# exit()

plt.scatter(x_values[:400], y_values[:400], s=10, c="red", alpha=1)
plt.savefig("yay_16.png")
plt.show()

# Figure out where the x values and y values are.
# 5 is the top of the screen. Use this to scale Y position correctly. # 12 is the bottom.
# Remove the outliers
