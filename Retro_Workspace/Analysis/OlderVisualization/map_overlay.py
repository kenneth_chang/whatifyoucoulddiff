import os, matplotlib, pickle
import numpy as np

import matplotlib.pyplot as plt

with open("Data/log_data.pickle", "rb") as f:
    log_dict = pickle.load(f)


# print(log_dict["x_y_level"])
# print(len(log_dict["x_y_level"]))
# print(log_dict.keys()) 

x_values = []
y_values = []
output_end = 7
output_start = 0

input_start = 5
input_end = 12
for map_x_y in log_dict["x_y_level"]:
    x_values.append(map_x_y[1])
    if map_x_y[0] == 50 and map_x_y[2] < 30: 
        output = output_start + ((output_end - output_start) / (input_end - input_start)) * (map_x_y[2] - input_start)
        y_values.append(output)

plt.scatter(x_values[:400], y_values[:400], c="red", alpha=1)
plt.savefig("yay.png")
plt.show()

# Figure out where the x values and y values are.
# 5 is the top of the screen. Use this to scale Y position correctly. # 12 is the bottom.
# Remove the outliers