import numpy as np
import os
from itertools import chain, combinations
import pickle
import collections

class KeyboardCodes:
    O = 111
    P = 112

    A = 97
    D = 100
    E = 101
    Q = 113
    S = 115
    W = 119
    X = 120
    Z = 122

    ENTER = 13

    L_SHIFT = 304
    SPACE = 32

    UP_ARROW = 273
    DOWN_ARROW = 274
    LEFT_ARROW = 275
    RIGHT_ARROW = 276
    
# ['B', 'Y', 'SELECT', 'START', 'UP',     'DOWN', 'LEFT', 'RIGHT', 'A', 'X',     'L', 'R']
class RetroSnesButtonMaps:
    IDLE = np.array([0,0,0,0,0, 0,0,0,0,0, 0,0])

    A = np.array([0,0,0,0,0, 0,0,0,1,0, 0,0])
    B = np.array([1,0,0,0,0, 0,0,0,0,0, 0,0])
    X = np.array([0,0,0,0,0, 0,0,0,0,1, 0,0])
    Y = np.array([0,1,0,0,0, 0,0,0,0,0, 0,0])

    L = np.array([0,0,0,0,0, 0,0,0,0,0, 1,0])
    R = np.array([0,0,0,0,0, 0,0,0,0,0, 0,1])
    
    UP    = np.array([0,0,0,0,1, 0,0,0,0,0, 0,0])
    DOWN  = np.array([0,0,0,0,0, 1,0,0,0,0, 0,0])
    RIGHT = np.array([0,0,0,0,0, 0,1,0,0,0, 0,0])
    LEFT  = np.array([0,0,0,0,0, 0,0,1,0,0, 0,0])
    
    SELECT = np.array([0,0,1,0,0, 0,0,0,0,0, 0,0])
    START  = np.array([0,0,0,1,0, 0,0,0,0,0, 0,0])
    
#['B', None, 'SELECT', 'START', 'UP',   'DOWN', 'LEFT', 'RIGHT', 'A']
class RetroGBButtonMaps:
    IDLE = np.array([0,0,0,0,0, 0,0,0,0])

    NONE = np.array([0,1,0,0,0, 0,0,0,0])

    SELECT = np.array([0,0,1,0,0, 0,0,0,0])
    START = np.array([0,0,0,1,0, 0,0,0,0])
    
    UP = np.array([0,0,0,0,1, 0,0,0,0])
    DOWN = np.array([0,0,0,0,0, 1,0,0,0])
    LEFT = np.array([0,0,0,0,0, 0,1,0,0])
    RIGHT = np.array([0,0,0,0,0, 0,0,1,0])

    A = np.array([0,0,0,0,0, 0,0,0,1])
    B = np.array([1,0,0,0,0, 0,0,0,0])

SNES_mapping = collections.OrderedDict()    

SNES_mapping[KeyboardCodes.ENTER]       = RetroSnesButtonMaps.START
SNES_mapping[KeyboardCodes.A]           = RetroSnesButtonMaps.Y
SNES_mapping[KeyboardCodes.Q]           = RetroSnesButtonMaps.L
SNES_mapping[KeyboardCodes.S]           = RetroSnesButtonMaps.X
SNES_mapping[KeyboardCodes.W]           = RetroSnesButtonMaps.R
SNES_mapping[KeyboardCodes.X]           = RetroSnesButtonMaps.A
SNES_mapping[KeyboardCodes.Z]           = RetroSnesButtonMaps.B
SNES_mapping[KeyboardCodes.UP_ARROW]    = RetroSnesButtonMaps.UP
SNES_mapping[KeyboardCodes.DOWN_ARROW]  = RetroSnesButtonMaps.DOWN
SNES_mapping[KeyboardCodes.LEFT_ARROW]  = RetroSnesButtonMaps.LEFT
SNES_mapping[KeyboardCodes.RIGHT_ARROW] = RetroSnesButtonMaps.RIGHT
SNES_mapping[KeyboardCodes.L_SHIFT]     = RetroSnesButtonMaps.SELECT

GB_mapping = collections.OrderedDict()

#['B', None, 'SELECT', 'START', 'UP', 'DOWN', 'LEFT', 'RIGHT', 'A']

GB_mapping[KeyboardCodes.SPACE]    = RetroGBButtonMaps.IDLE

GB_mapping[KeyboardCodes.A]    = RetroGBButtonMaps.SELECT
GB_mapping[KeyboardCodes.S]    = RetroGBButtonMaps.START
GB_mapping[KeyboardCodes.X]    = RetroGBButtonMaps.A
GB_mapping[KeyboardCodes.Z]    = RetroGBButtonMaps.B

GB_mapping[KeyboardCodes.UP_ARROW]    = RetroGBButtonMaps.UP
GB_mapping[KeyboardCodes.DOWN_ARROW]  = RetroGBButtonMaps.DOWN
GB_mapping[KeyboardCodes.LEFT_ARROW]  = RetroGBButtonMaps.RIGHT
GB_mapping[KeyboardCodes.RIGHT_ARROW] = RetroGBButtonMaps.LEFT

def powerset2(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    result = []
    for r in range(len(s)+1):
        result.append(combinations(s, r))
    return chain.from_iterable(result)

all_keys = list(powerset2(SNES_mapping.keys()))
all_keys_GB = list(powerset2(GB_mapping.keys()))

all_values = []
for key_group in all_keys:
    base_value = np.array([0,0,0,0,0, 0,0,0,0,0, 0,0])
    for key in key_group:
        base_value = base_value | SNES_mapping[key]
    all_values.append(base_value)

all_values_GB = []
for key_group in all_keys_GB:
    base_value = np.array([0,0,0,0,0, 0,0,0,0])
    for key in key_group:
        base_value = base_value | GB_mapping[key]
    all_values_GB.append(base_value)

SNES_full_mapping = {tuple(key,):value for key, value in zip(all_keys, all_values)}
GB_full_mapping = {tuple(key,):value for key, value in zip(all_keys_GB, all_values_GB)}

print(GB_full_mapping.keys())
print(len(GB_full_mapping.keys()))
if not os.path.exists("KeyMappings"):
        os.makedirs("KeyMappings")

with open("KeyMappings/SNES_button_mapping.pickle", "wb") as f:
    pickle.dump(SNES_full_mapping, f)

with open("KeyMappings/GB_button_mapping.pickle", "wb") as f:
    pickle.dump(GB_full_mapping, f)
