import os
import glob
import math
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

#img_src is hardcoded just for Testing
img_src = '/home/ken/WhatIfYouCouldDiff/Retro_Workspace/ExperimentResults/SuperMarioWorld-Snes/ken_is_testing/HumanPlay/Images'
a_font_files = [os.path.join(img_src, f) for f in os.listdir(img_src) if f.endswith(".png")]

"""
Principal Component Analysis
input: X, matrix with trainnig data stored as flattened arrays in rows
return: projection matrix (with important dimensions first), variance and mean.

SVD factorization:  A = U * Sigma * V.T
                    A.T * A = V * Sigma^2 * V.T  (V is eigenvectors of A.T*A)
                    A * A.T = U * Sigma^2 * U.T  (U is eigenvectors of A * A.T)
                    A.T * U = V * Sigma
"""

def pca(X):
    num_data, dim = X.shape

    mean_X = X.mean(axis=0)
    X = X - mean_X

    if dim > num_data:
        #Use PCA compact trick?
        M = np.dot(X, X.T) #Covariance matrix
        e, U = np.linalg.eigh(M) #Calculate eigenvalues and deigenvectors
        tmp = np.dot(X.T, U).T
        V = tmp[::-1] #reverse since we want the last eigenvectors
        S = np.sqrt(e)[::-1] #reverse since the last eigenvalues are in increasing order
        for i in range(V.shape[1]):
            V[:,i] /= S
    else:
        # normal PCA, SVD method
        U,S,V = np.linalg.svd(X)
        V = V[:num_data] # only makes sense to return the first num_data
    return V, S, mean_X

if __name__ == "__main__":
    img_matrix = np.array([np.array(Image.open(im, 'r')).flatten()
                 for im in a_font_files], 'f')

    #Do PCA
    V, S, immean = pca(img_matrix)

    # Show Results
    # First one is the mean image
    # Rest 7 are the top 7 features extracted
    tmp_img = np.array(Image.open(a_font_files[0], 'r'))
    m,n = tmp_img.shape
    fig = plt.figure()
    plt.gray()

    plt.subplot(3,4,1)
    plt.imshow(immean.reshape(m,n))
    plt.axis('off')

    for i in range(11):
        plt.subplot(3,4,i+2)
        plt.imshow(V[i].reshape(m,n))
        plt.axis('off')

    plt.show()
