import time
import os, os.path
import random
import cv2
import glob
import keras
import matplotlib
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.cluster import KMeans
from sklearn.mixture import GaussianMixture
from sklearn.decomposition import PCA

import pandas as pd
import numpy as np

img_src = '/home/ken/WhatIfYouCouldDiff/Retro_Workspace/ExperimentResults/SuperMarioWorld-Snes/ken_is_testing/HumanPlay/Images'
img_files = [os.path.join(img_src, f) for f in os.listdir(img_src) if f.endswith(".png")]

def dataset_stats():

    # This is an array with the letters available.
    # If you add another animal later, you will need to structure its images in the same way
    # and add its letter to this array
    animal_characters = ['C', 'D']

    # dictionary where we will store the stats
    stats = []

    for animal in img_files:
        # get a list of subdirectories that start with this character
        file_names = [file for file in os.listdir(img_src)]
        file_count = len(file_names)
        sub_directory_name = os.path.basename(img_src)
        stats.append({ "Code": sub_directory_name[:img_src.find('-')],
                        "Image count": file_count,
                       "Folder name": os.path.basename(img_src),
                        "File names": file_names})


    df = pd.DataFrame(stats)

    return df

def load_images(codes):

    # Define empty arrays where we will store our images and labels
    images = []
    labels = []

    for code in codes:
        # get the folder name for this code
        folder_name = dataset.loc[code]["Folder name"]

        for file in dataset.loc[code]["File names"]:
            # build file path
            file_path = os.path.join(DIR, folder_name, file)

            # Read the image
            image = cv2.imread(file_path)

            # Resize it to 224 x 224
            image = cv2.resize(image, (224,224))

            # Convert it from BGR to RGB so we can plot them later (because openCV reads images as BGR)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

            # Now we add it to our array
            images.append(image)
            labels.append(code)

    return images, labels

def normalise_images(images, labels):

    # Convert to numpy arrays
    images = np.array(images, dtype=np.float32)
    labels = np.array(labels)

    # Normalise the images
    images /= 255

    return images, labels

if __name__ == "__main__":
    d = dataset_stats()
    print(d)
    images, labels = load_images("1")
    images, labels = normalise_images(images, labels)
