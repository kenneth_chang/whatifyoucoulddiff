import gym

env = gym.make("CarRacing-v0")
env.reset()
while True:
   env.step(env.action_space.sample())
   env.render()
