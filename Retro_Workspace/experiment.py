import retro

import time
import random

# Make sure this line is above importing save hyperparams??
from keras.models import load_model
from retro_engine.support_utils import save_hyperparameters

from retro_engine.experiment_management import experiment_setup
from retro_engine.experiment_management import calculate_score
from retro_engine.experiment_management import make_gif, make_gif_from_array

from retro_engine.algorithms import random_play, rrt_play, forest_play

from retro_engine.visualization import visualize_results

import pickle, glob
import argparse
import shutil
import os, sys, six
import numpy as np
from scipy.misc import imsave
import shutil

os.environ['TF_CPP_MIN_LOG_LEVEL'] = "3"

parser = argparse.ArgumentParser()
parser.add_argument('-a', '--algorithm', required=True, action='store', help='Which algorithm to use. rrt, forest, random, chaos or mcts.')
parser.add_argument('-t', '--run_time', type=int,  default=1, action='store', help='Run time in minutes you wish to give each state')
parser.add_argument('-m', '--movie_name', action='store', help='The name of the bk.2 file to load. No path or extension is needed. If this is not given RRT starts from game start.')
parser.add_argument('-p', '--specific_pickle', type=int, action='store', help='The state count of the pickle to be used. Just the integer. Required if there are more than one pickle file.')
parser.add_argument('-s', '--use_start_only', action='store_true', help='Whether RRT only uses the start of the movie or the whole movie. Set this for RRT only.')
parser.add_argument('-g', '--game_name', action='store', default="SuperMarioWorld-Snes", help='The name of the game. Must use the directory names under retro/lib/python3.6/site-packages/retro/data/stable.')
parser.add_argument('-no', '--norender',  action='store_true', default=False, help='Disables rendering of the game during state collection')
parser.add_argument('-seed', '--seed',  action='store', help='The seed to be used. If not set, it is randomly.')
parser.add_argument('-i', '--img_save_count', type=int, action='store', default=100, help='How many steps between saving images. Defaults to 100')
parser.add_argument('-gif', '--make_gif', action='store_true', default=False, help='Creates a gif of the playthrough.')

args = parser.parse_args()

if __name__ == "__main__":


    states, result_dir_path, plot_title = experiment_setup(args)
    env = retro.make(game=args.game_name, use_restricted_actions=retro.Actions.ALL, state=None, record=result_dir_path + "/Movie")

    # Set the random seeds.
    seed = args.seed if args.seed else random.randint(0, 10000)
    np.random.seed = seed
    random.seed = seed

    total_run_time = 60 * args.run_time
    start_time_ascii = time.asctime(time.localtime(time.time()))
    print("\n########################################################################")
    print(f"#\n# RRT Starting on {args.game_name} with the {args.algorithm} method.")
    print(f"# From the movie: {args.movie_name} with {len(states)} states.")
    print(f"# Only using the start state: {args.use_start_only}.")
    print(f"# The run time is: {args.run_time} minutes.")
    print(f"# Experiment start time: {start_time_ascii}")
    print(f"# The seed is {seed}.")
    print("# ")
    print(f"# The raw args :")
    print(f"#               --algorithm: {args.algorithm}")
    print(f"#               --run_time: {args.run_time}")
    print(f"#               --movie_name: {args.movie_name}")
    print(f"#               --specific_pickle: {args.specific_pickle}")
    print(f"#               --use_start_only: {args.use_start_only}")
    print(f"#               --game_name: {args.game_name}")
    print(f"#               --norender: {args.norender}")
    print(f"#               --seed: {args.seed}")
    print(f"#               --img_save_count: {args.img_save_count}")
    print("# ")
    print("########################################################################\n")

    save_hyperparameters(["retro_engine/algorithms.py"], f"{result_dir_path}/code_snapshot.txt")

    start_time = time.time()

    #####################
    # START THE ENGINE. #
    #####################
    if args.algorithm.lower() == "rrt":
        logging_dict = rrt_play(total_run_time, env, states, args, result_dir_path)
    elif args.algorithm.lower() == "forest":
        logging_dict = forest_play(total_run_time, env, states, args, result_dir_path)
    elif args.algorithm.lower() == "random":
        logging_dict = random_play(total_run_time, env, states, args, result_dir_path)
    elif args.algorithm.lower() == "chaos":
        logging_dict = random_play(total_run_time, env, states, args, result_dir_path, dist=True)
    end_time = time.time()

    logging_dict['algorithm'] = args.algorithm.lower()
    logging_dict['game_name'] = args.game_name
    logging_dict['movie_name'] = args.movie_name

    #####################
    # GET THE SCORE.    #
    #####################
    logging_dict = calculate_score(logging_dict)

    print("\n######################## RESULTS ######################################")
    print("# ")
    print(f"# The final BBsum metric is {logging_dict['bbox_sum'][-1]:.2f}.")
    print(f"# The final Nuclear Norm metric {logging_dict['nuc_norm'][-1]:.2f}.")
    print("# ")
    print(f"# Experiment end time: {time.asctime(time.localtime(time.time()))}")
    print(f"# Total run time: {end_time - start_time}")
    print(f"# Total steps: {logging_dict['steps']}")
    print("########################################################################\n")

    #####################
    # LOG THE DATA.     #
    #####################
    pickle_path = result_dir_path + "/log_data.pickle"
    with open(pickle_path, 'wb') as f:
        pickle.dump(logging_dict, f,  pickle.DEFAULT_PROTOCOL)

    with open(os.path.join(f"{result_dir_path}", "README.txt"), "w") as readme:
        print("\n########################################################################", file=readme)
        print(f"#\n# RRT Starting on {args.game_name} with the {args.algorithm} method.", file=readme)
        print(f"# From the movie: {args.movie_name} with {len(states)} states.", file=readme)
        print(f"# Only using the start state: {args.use_start_only}.", file=readme)
        print(f"# The given run time is: {args.run_time} minutes.", file=readme)
        print(f"# Experiment start time: {start_time_ascii}", file=readme)
        print(f"# The seed is {seed}.", file=readme)
        print("# ", file=readme)
        print(f"# The final BBsum metric is {logging_dict['bbox_sum'][-1]:.2f}.", file=readme)
        print(f"# The final Nuclear Norm metric {logging_dict['nuc_norm'][-1]:.2f}.", file=readme)
        print("# ", file=readme)
        print(f"# Experiment end time: {time.asctime(time.localtime(time.time()))}", file=readme)
        print(f"# Total run time: {end_time - start_time}", file=readme)
        print(f"# Total steps: {logging_dict['steps']}", file=readme)
        if "rrt_expansions" in logging_dict.keys():
            print(f"# Total rrt expansions: {logging_dict['rrt_expansion']}", file=readme)
        print("# ", file=readme)
        print(f"# The raw args :", file=readme)
        print(f"#               --run_time: {args.run_time}", file=readme)
        print(f"#               --movie_name: {args.movie_name}", file=readme)
        print(f"#               --use_start_only: {args.use_start_only}", file=readme)
        print(f"#               --game_name: {args.game_name}", file=readme)
        print(f"#               --norender: {args.norender}", file=readme)
        print(f"#               --seed: {args.seed}", file=readme)
        print(f"#               --img_save_count: {args.img_save_count}", file=readme)
        print("########################################################################\n", file=readme)


    if args.make_gif:
        make_gif(f"{args.algorithm}-{args.movie_name}-{args.run_time}_mins" , result_dir_path)
        # make_gif_from_array(f"{args.algorithm}-{args.movie_name}-{args.run_time}_mins" , result_dir_path, logging_dict["all_images"])
    visualize_results(logging_dict, plot_title, result_dir_path)

    # Remove the incomplete tag.
    shutil.move(result_dir_path, result_dir_path[:-11])

    env.close()
# BREADCRUMBS_END
