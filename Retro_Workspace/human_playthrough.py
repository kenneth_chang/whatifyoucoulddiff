import retro
import argparse, pickle, os, time
from scipy.misc import imsave
from keras.models import load_model

from retro_engine.human_play import play

from retro_engine.experiment_management import human_experiment_create_folders
from retro_engine.experiment_management import human_experiment_record_data
from retro_engine.experiment_management import calculate_score
from retro_engine.experiment_management import make_gif

from retro_engine.visualization import visualize_results

import numpy as np

TILE_SIZE = 16

def human_play(env) -> dict:
    global save_time, start_time
    result_dir_image_path = data_dir_path + f"/Images"
    if args.ram_save_count:
        ram_file_handle = open(f"{data_dir_path}/Ram/{args.game_name}_ram.npy", 'ba')

    save_time = time.time()


    def set_embedding():
        if "Mario" in env.gamename:
            embedding_model = load_model(f"retro_engine/Models/{args.game_name}.h5")             # Used as in the embedding model.
        elif "Zelda" in env.gamename:
            with open(f"retro_engine/Models/PCA_Zelda_30min_64.pickle", 'rb') as f:
                embedding_model = pickle.load(f)
        return embedding_model

    def get_embedding(obs):
        if "Mario" in env.gamename:
            return embedding_model.predict(obs[None,:]).squeeze()             # Used as in the embedding model.
        elif "Zelda" in env.gamename:
            return embedding_model.transform(env.get_ram()[None, 0:8192]).squeeze()

    embedding_model = set_embedding()             # Used as in the embedding model.

    logging_dict = dict()

    _obs = env.reset()
    embeddings = []
    steps = 0
    x_y_level = set()
    all_possibilities_len = []
    rams = []

    start_time = time.time()
    ram_save_counter = 0
    for _obs, _info in play(env, zoom=3):
        if "SuperMarioWorld" in env.gamename:
            # print(_info["level"], _info["x_position"]//TILE_SIZE, _info["y_position"]//TILE_SIZE)
            x_y_level.add((_info["level"], _info["x_position"]//TILE_SIZE, _info["y_position"]//TILE_SIZE))

        if "Zelda" in env.gamename:
            # print(_info["dungeonindex"],_info["worldindex"], _info["xPosLink"]//TILE_SIZE, _info["yPosLink"]//TILE_SIZE)
            x_y_level.add((_info["dungeonindex"],_info["worldindex"], _info["xPosLink"]//TILE_SIZE, _info["yPosLink"]//TILE_SIZE))

        if steps % args.img_save_count == 0:
            filename = f"Screencap_{steps}.png"
            imsave(os.path.join(result_dir_image_path, filename), _obs)
            embeddings.append(get_embedding(_obs))
            all_possibilities_len.append(len(x_y_level))

        if args.ram_save_count and steps % args.ram_save_count == 0:
            ram = env.get_ram()
            rams.append(ram)
            ram_save_counter += 1
            if ram_save_counter > 100:
                rams_temp = np.array(rams)
                np.save(ram_file_handle, rams_temp)
                rams.clear()
                ram_save_counter += 0

        if(count_save and steps % args.save_count == 0):
            statetest = env.em.get_state()
            states.append(statetest)
            print(f"State {len(states)} has been saved.")

        if(frequency_save and time.time() > save_time):
            save_time = time.time() + args.snapshot_save_frequency
            statetest = env.em.get_state()
            states.append(statetest)
            print(f"State {len(states)} has been saved.")
        steps += 1

    rams = np.array(rams)
    np.save(f"{result_dir_path}/{args.game_name}_ram.npy", rams)
    logging_dict["all_embeddings"] = embeddings
    logging_dict["steps"] = steps
    logging_dict["x_y_level"] = x_y_level
    logging_dict["all_possibilities_len"] = all_possibilities_len
    logging_dict["num_states"] = len(states)
    if args.ram_save_count:
        ram_file_handle.close()
    return logging_dict


if __name__ == "__main__":
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = "3"

    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--movie_name', required=True, action='store', help='The name of the movie file to save. Do not give a path or extension.')
    parser.add_argument('-g', '--game_name', action='store', default="SuperMarioWorld-Snes", help='The name of the game. Used as a part of the movie folder. Defaults to SuperMarioWorld-Snes')
    parser.add_argument('-f', '--snapshot_save_frequency',  action='store', type=float, default=0, help='How much time passes between each save in seconds.')
    parser.add_argument('-c', '--snapshot_save_count',  action='store', type=float, default=0, help='Save an exact amount of snapshots.')
    parser.add_argument('-r', '--ram_save_count',  action='store', type=float, default=0, help='How many steps between saving ram state. Default is 0.')
    parser.add_argument('-i', '--img_save_count',  action='store', default=60, type=int, help='How many steps between saving game screenshots. Default is 60')
    parser.add_argument('-s', '--see_states',  action='store_true', default=True, help='See the states after creation.')
    parser.add_argument('-t', '--testing',  action='store_true', default=False, help='Testing work!')
    parser.add_argument('-gif', '--make_gif', action='store_true', default=False, help='Creates a gif of the playthrough.')


    args = parser.parse_args()
    if args.snapshot_save_count and args.snapshot_save_frequency:
        parser.error("Please use save_count OR save_frequency.")

    if not (args.snapshot_save_count or args.snapshot_save_frequency):
        parser.error("Please give either a save_count or a save_frequency.")

    frequency_save = bool(args.snapshot_save_frequency)
    count_save = bool(args.snapshot_save_count)

    result_dir_path, data_dir_path = human_experiment_create_folders(args.game_name, args.movie_name, args.testing)
    env = retro.make(game=args.game_name, use_restricted_actions=retro.Actions.ALL, state=None, record=data_dir_path + "/Movie")
    env.reset()

    steps = 0
    states = []
    save_time = time.time()

    start_time = time.time()
    #####################
    # START THE ENGINE. #
    #####################
    logging_dict = human_play(env)
    logging_dict['algorithm'] = "human"
    logging_dict['game_name'] = args.game_name
    logging_dict['movie_name'] = args.movie_name

    logging_dict = calculate_score(logging_dict)
    visualize_results(logging_dict, f"{args.movie_name} Human Play", result_dir_path)
    human_experiment_record_data(logging_dict, states, start_time)

    if args.make_gif:
        make_gif(f"human-{args.movie_name}", data_dir_path)

    if args.see_states:
        active_state_count = -1
        print("\nEither insert state number or enter to go to the next one.")
        while True:
            state_num = input(f"Currently at index {active_state_count} of states list of len {len(states)}. ")
            if state_num:
                active_state_count = int(state_num)
            else:
                if active_state_count < (len(states) - 1):
                    active_state_count += 1
                else:
                    print("At last state!")
            env.em.set_state(states[active_state_count])
            obs, _, _, _ = env.step(env.action_space.sample())
            env.render()

    env.close()
