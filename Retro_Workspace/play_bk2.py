import retro
import os
import argparse
import time
import imageio
import cv2
import numpy as np

os.environ['TF_CPP_MIN_LOG_LEVEL'] = "3"

parser = argparse.ArgumentParser()
parser.add_argument('-m', '--movie_name', required=True, action='store', help='The name of the movie to watch.')
parser.add_argument('-g', '--game_name', action='store', default="SuperMarioWorld-Snes", help='The name of the game. Used as a part of the movie folder.')
parser.add_argument('-e', '--experiment_number', action='store', type=int, default=0, help='The number of the experiment. If 0, loads human play.')
parser.add_argument('-s', '--slow_mode', action='store_true', default=False, help='Runs the recording in near realtime.')
parser.add_argument('-gif', '--make_gif', action='store_true', default=False, help='Creates a gif of the playthrough.')
args = parser.parse_args()

if args.experiment_number:
    movie_name = f"ExperimentResults/{args.game_name}/{args.movie_name}/experiment_{args.experiment_number}/Movie/{args.game_name}-Opening-000169.bk2"
else:
    movie_name = f"Data/{args.game_name}/{args.movie_name}/Movie/{args.game_name}-Opening-000169.bk2"


movie = retro.Movie(movie_name)

# Check the official documentation to see if there is initial "step"
movie.step()

env = retro.make(
    game=movie.get_game(),
    state=movie.get_state(),
    # bk2s can contain any button presses, so allow everything
    use_restricted_actions=retro.Actions.ALL,
    players=movie.players,
)
env.initial_state = movie.get_state()
env.reset()

gif_buffer = []
while movie.step():
    keys = []
    for p in range(movie.players):
        for i in range(env.num_buttons):
            keys.append(movie.get_key(i, p))
    env.step(keys)
    env.render()
    if args.make_gif:
        image = env.em.get_screen()
        gif_buffer.append(image)
    if args.slow_mode:
        time.sleep(1/120)

if args.make_gif:
    # If you want gif instead you can use this.
    #* imageio.mimsave("test.gif", gif_buffer[::60], duration=1/60)

    gif_buffer = np.array(gif_buffer)

    # Arcane numpy trickery to change the channel ordering from RGB to BGR
    gif_buffer = gif_buffer[...,::-1]

    height, width = gif_buffer.shape[1], gif_buffer.shape[2]
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    video = cv2.VideoWriter(f'{movie_name}.mp4', fourcc, float(60), (width, height))
    for image in gif_buffer:
        video.write(image)
    video.release()

