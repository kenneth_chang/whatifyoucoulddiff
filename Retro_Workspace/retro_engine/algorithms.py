# Make it so that this is not imported in MCTS
from keras.models import load_model
import pickle, glob
import argparse
import shutil
import os, sys, six
import numpy as np
from scipy.misc import imsave
import time
import random


TILE_SIZE = 32

# BREADCRUMBS_START
def random_play(total_run_time:int, env, states:np.ndarray, args, result_dir_path, dist=None) -> dict:
    result_dir_image_path = result_dir_path + f"/Images"
    start_time = time.time()

    pix2mem_model = load_model(f"retro_engine/Models/{args.game_name}.h5")             # Used as in the embedding model.

    steps = 0                           # How many frames were stepped through.
    state_switch_iteration_counts = []  # When did the next state from the movie was loaded?
    all_embeddings = []                 # All the embeddings that were found in this run.
    all_embeddings_images = []

    def get_embedding(obs):
        if "SuperMario" in env.gamename:
            return pix2mem_model.predict(obs[None,:]).squeeze()
        else:
            return np.zeros((256,))

    if dist:
        distribution = np.load("retro_engine/Distributions/Zelda_Retro_Dist.npy")
    
    def get_action():
        
        if dist:
            temp_action = np.random.choice(4096, p=distribution)
            # Convert the action int to a binary representation of that number
            str_action = f'{temp_action:b}'.rjust(12, '0')[::-1]
            
            # Make the list int
            action = [int(str_button) for str_button in str_action]
        else: 
            action = env.action_space.sample()
        return action

    cold_start = not bool(states)
    if cold_start:
        states = [0]

    obs = env.reset()
    state_run_length = total_run_time / len(states)

    x_y_level = set()
    all_possibilities_len = []
    
    num_saves_in_this_save_state = 0
    for idx, start_point in enumerate(states):
        if not cold_start:
            env.em.set_state(start_point)

        observations = [obs]
        all_embeddings.append(get_embedding(obs))
        explore_count = [0]

        t_end = time.time() + state_run_length

        while time.time() < t_end:
            chosen_action = get_action()
            for _ in range(4):
                obs, _, _, _info = env.step(chosen_action)
                if "SuperMarioWorld" in env.gamename:
                    x_y_level.add((_info["level"], _info["x_position"]//TILE_SIZE, _info["y_position"]//TILE_SIZE))

                if "Zelda" in env.gamename:
                    x_y_level.add((_info["dungeonindex"],_info["worldindex"], _info["xPosLink"]//TILE_SIZE, _info["yPosLink"]//TILE_SIZE))
                
                # Consider saving every step.
                if(steps % args.img_save_count == 0):
                    all_embeddings_images.append(get_embedding(obs))
                    filename = f"Screencap_{steps}.png"
                    imsave(os.path.join(result_dir_image_path, filename), obs)
                    all_possibilities_len.append(len(x_y_level))
                    num_saves_in_this_save_state += 1
            steps += 1
            if(not args.norender):
                env.render()
            
            explore_count.append(1)
            observations.append(obs)

        state_switch_iteration_counts.append(num_saves_in_this_save_state)

    logging_dict = dict()
    logging_dict["steps"] = steps
    logging_dict["state_switch_iteration_counts"] = state_switch_iteration_counts
    logging_dict["all_embeddings"] = all_embeddings_images
    logging_dict["x_y_level"] = x_y_level
    logging_dict["all_possibilities_len"] = all_possibilities_len
    return logging_dict

def rrt_play(total_run_time:int, env, states:np.ndarray, args, result_dir_path) -> dict:
    save_at_expansion = True
    result_dir_image_path = result_dir_path + f"/Images"
    start_time = time.time()

    ACTION_NUM = 4096                                           
    
    steps = 0                           # How many frames were stepped through.
    rrt_expansion = 0                   # How many steps RRT expanded nodes.
    state_switch_iteration_counts = []  # When did the next state from the movie was loaded?
    steps_per_iteration = []            # How long did each iteration take.
    all_embeddings = []                 # All the embeddings that were found in this run.
    all_embeddings_images = []    
    all_distances = []

    def action_policy_act(obs):
        temp_action = np.random.choice(ACTION_NUM, p=distribution)

        # Convert the action int to a binary representation of that number
        str_action = f'{temp_action:b}'.rjust(12, '0')[::-1]
        
        # Make the list int
        action = [int(str_button) for str_button in str_action]
        return action

    def set_distribution():
        if "Mario" in env.gamename: 
            distribution = np.load("retro_engine/Distributions/SNES_retro_dist.npy")        # Used as in the embedding model.
        elif "Zelda" in env.gamename:
            distribution = np.load("retro_engine/Distributions/Zelda_Retro_Dist.npy")
        return distribution

    def set_embedding():
        if "Mario" in env.gamename: 
            embedding_model = load_model(f"retro_engine/Models/{args.game_name}.h5")             # Used as in the embedding model.
        elif "Zelda" in env.gamename:
            with open(f"retro_engine/Models/PCA_Zelda_30min_64.pickle", 'rb') as f:
                embedding_model = pickle.load(f) 
        return embedding_model

    def get_embedding(obs):
        if "Mario" in env.gamename: 
            return embedding_model.predict(obs[None,:]).squeeze()             # Used as in the embedding model.
        elif "Zelda" in env.gamename:
            return embedding_model.transform(env.get_ram()[None, 0:8192]).squeeze()
    
    def get_random_goal(obs, max_val, min_val):

        # Returns an array.
        # Conceptually what we wanted
        # np.random.uniform(size=obs.shape, low=min_val, high=max_val)
        
        # Returns a single number which is broadcasted ends up being an array of the single number.
        # Seems to work better, probably due to picking things on the fringes.
        # np.random.uniform(low=min_val, high=max_val)

        return np.random.uniform(size=obs.shape, low=min_val, high=max_val)

    def get_distance(em_obs, goal):
        return np.linalg.norm(em_obs - goal)

    embedding_model = set_embedding()             # Used as in the embedding model.
    distribution = set_distribution() # Used for chaos monkey action selection

    if "SuperMarioWorld" in env.gamename:
        action_granularity = 6
    elif "Zelda" in env.gamename: 
        action_granularity = 60

    obs = env.reset()

    initial_em = get_embedding(obs)
    max_embedding_val = np.array(initial_em)
    min_embedding_val = np.array(initial_em)

    cold_start = not bool(states)
    if cold_start:
        states = [0]
    state_run_length = total_run_time / len(states)

    x_y_level = set()
    all_possibilities_len = []


    for idx, start_point in enumerate(states):
        if not cold_start:
            env.em.set_state(start_point)
        observations = [obs]
        embeddings = [get_embedding(obs)]
        all_embeddings.append(get_embedding(obs))
        save_states = [env.em.get_state()]
        explore_count = [0]
        state_switch_iteration_counts.append(rrt_expansion)

        t_end = time.time() + state_run_length
        all_explore_counts = []

        # print(f"We are in state {idx+1}")
        while time.time() < t_end:
            if(save_at_expansion):
                filename = f"Expansion_{rrt_expansion}.png"
                imsave(os.path.join(result_dir_image_path, filename), obs)
            rrt_expansion += 1

            # filename = f"Screencap_{steps}.png"
            # imsave(os.path.join(result_dir_image_path, filename), obs)

            # Convert the observation into embeddings
            em_ob = get_embedding(obs)

            goal = get_random_goal(em_ob, max_embedding_val, min_embedding_val)

            distances = [get_distance(em_obs, goal) + (explore_count[index])**2 for index, em_obs in enumerate(embeddings)] 
            chosen_index = np.argmin(distances)

            explore_count[chosen_index] += 1

            # Load the closest goal.
            chosen_state = save_states[chosen_index]
            env.em.set_state(chosen_state)

            # Get the correct embedding
            chosen_observation = get_embedding(observations[chosen_index])

            # Take that action action step times (usually 30)
            # random.randint(4, 30)
            for _ in range(400 // action_granularity):
                if time.time() > t_end:
                    break
                chosen_action = action_policy_act(chosen_observation)
                if "Zelda" in env.gamename:
                    action_granularity = random.randint(1, action_granularity)
                for _ in range(action_granularity):
                    if(not args.norender):
                        env.render()
                    obs, _, _, _info = env.step(chosen_action)
                    if "SuperMarioWorld" in env.gamename:
                        # print(_info["level"], _info["x_position"]//TILE_SIZE, _info["y_position"]//TILE_SIZE)
                        x_y_level.add((_info["level"], _info["x_position"]//TILE_SIZE, _info["y_position"]//TILE_SIZE))

                    if "Zelda" in env.gamename:
                        x_y_level.add((_info["dungeonindex"],_info["worldindex"], _info["xPosLink"]//TILE_SIZE, _info["yPosLink"]//TILE_SIZE))

                    steps += 1

                    #* Gif making code
                    # if gif and len(obs.shape) == 3:
                    #     all_images.append(obs)
                    # else:
                    #     all_images.append(env.em.get_screen())

                    # Consider saving every step.
                    if(steps % args.img_save_count == 0):
                        all_embeddings_images.append(get_embedding(obs))
                        filename = f"Screencap_{steps}.png"
                        imsave(os.path.join(result_dir_image_path, filename), obs)
                        all_possibilities_len.append(len(x_y_level))

                    if(steps % 5000 == 0):
                        print(f"Total seconds since start: {time.time() - start_time:.1f}")

            # if(not args.norender):
            #     env.render()

            
            all_explore_counts.append(tuple(explore_count))
            explore_count.append(1)

            observations.append(obs)
            embedded_obs = get_embedding(obs)
            embeddings.append(embedded_obs)
            all_embeddings.append(embedded_obs)
            steps_per_iteration.append(steps)
            save_states.append(env.em.get_state())
            all_distances.append(distances)

            np.maximum(max_embedding_val, embedded_obs, out=max_embedding_val)
            np.minimum(min_embedding_val, embedded_obs, out=min_embedding_val)

    # Todo: Abstract Away Logging Dict
    logging_dict = dict()
    logging_dict["steps"] = steps
    logging_dict["rrt_expansion"] = rrt_expansion
    logging_dict["state_switch_iteration_counts"] = state_switch_iteration_counts
    logging_dict["steps_per_iteration"] = steps_per_iteration
    logging_dict["x_y_level"] = x_y_level
    logging_dict["all_embeddings"] = all_embeddings_images
    logging_dict["rrt_embeddings"] = embeddings
    logging_dict["all_possibilities_len"] = all_possibilities_len
    logging_dict["end_time"] = time.time()
    logging_dict["all_explore_counts"] = all_explore_counts
    logging_dict["all_distances"] = all_distances

    return logging_dict

def forest_play(total_run_time:int, env, states:np.ndarray, args, result_dir_path) -> dict:
    result_dir_image_path = result_dir_path + f"/Images"
    ACTION_NUM = 4096                                           
    start_time = time.time()
    
    steps = 0                           # How many frames were stepped through.
    rrt_expansion = 0                   # How many steps RRT expanded nodes.
    state_switch_iteration_counts = []  # When did the next state from the movie was loaded?
    steps_per_iteration = []            # How long did each iteration take.
    all_embeddings = []                 # All the embeddings that were found in this run.
    all_embeddings_images = []    

    def action_policy_act(obs):
        temp_action = np.random.choice(ACTION_NUM, p=distribution)

        # Convert the action int to a binary representation of that number
        str_action = f'{temp_action:b}'.rjust(12, '0')[::-1]
        
        # Make the list int
        action = [int(str_button) for str_button in str_action]
        return action

    def set_distribution():
        if "Mario" in env.gamename: 
            distribution = np.load("retro_engine/Distributions/SNES_retro_dist.npy")        # Used as in the embedding model.
        elif "Zelda" in env.gamename:
            distribution = np.load("retro_engine/Distributions/Zelda_Retro_Dist.npy")
        return distribution

    def set_embedding():
        if "Mario" in env.gamename: 
            embedding_model = load_model(f"retro_engine/Models/{args.game_name}.h5")             # Used as in the embedding model.
        elif "Zelda" in env.gamename:
            with open(f"retro_engine/Models/PCA_Zelda_30min_64.pickle", 'rb') as f:
                embedding_model = pickle.load(f) 
        return embedding_model

    def get_embedding(obs):
        if "Mario" in env.gamename: 
            return embedding_model.predict(obs[None,:]).squeeze()             # Used as in the embedding model.
        elif "Zelda" in env.gamename:
            return embedding_model.transform(env.get_ram()[None, 0:8192]).squeeze()
    
    def get_random_goal(obs, max_val, min_val):

        # Returns an array.
        # Conceptually what we wanted
        # np.random.uniform(size=obs.shape, low=min_val, high=max_val)
        
        # Returns a single number which is broadcasted ends up being an array of the single number.
        # Seems to work better, probably due to picking things on the fringes.
        # np.random.uniform(low=min_val, high=max_val)

        return np.random.uniform(size=obs.shape, low=min_val, high=max_val)

    def get_distance(em_obs, goal):
        return np.linalg.norm(em_obs - goal)

    embedding_model = set_embedding()             # Used as in the embedding model.
    distribution = set_distribution() # Used for chaos monkey action selection

    if "SuperMarioWorld" in env.gamename:
        action_granularity = 6
    elif "Zelda" in env.gamename: 
        action_granularity = 40

    obs = env.reset()

    max_embedding_val = np.full(get_embedding(obs).shape, -99999.0)
    min_embedding_val = np.full(get_embedding(obs).shape,  99999.0)

    x_y_level = set()
    all_possibilities_len = []

    observations = []
    embeddings = []
    for state in states:
        env.em.set_state(state)
        if "Zelda" in env.gamename:
            obs = env.get_ram()
        else:
            obs = env.get_screen()
        chosen_observation = get_embedding(obs)
        embeddings.append(chosen_observation)
        observations.append(obs)
    save_states = states
    random.shuffle(save_states, lambda: .5)
    explore_count = [1 for obs in states]

    t_end = time.time() + total_run_time

    # print(f"We are in state {idx+1}")
    while time.time() < t_end:
        rrt_expansion += 1

        # filename = f"Screencap_{steps}.png"
        # imsave(os.path.join(result_dir_image_path, filename), obs)

        # Convert the observation into embeddings
        em_ob = get_embedding(obs)

        goal = get_random_goal(em_ob, max_embedding_val, min_embedding_val)

        chosen_index = np.argmin([get_distance(em_obs, goal) + (explore_count[index])**2 for index, em_obs in enumerate(embeddings)])
        explore_count[chosen_index] += 1

        # Load the closest goal.
        chosen_state = save_states[chosen_index]
        env.em.set_state(chosen_state)

        # Get the correct embedding
        if "Zelda" in env.gamename:
            obs = env.get_ram()
        else:
            obs = env.get_screen()
        chosen_observation = get_embedding(obs)

        # Take that action action step times (usually 30)
        # random.randint(4, 30)
        for _ in range(400 // action_granularity):
            chosen_action = action_policy_act(chosen_observation)
            for _ in range(action_granularity):
                if(not args.norender):
                    env.render()
                obs, _, _, _info = env.step(chosen_action)
                if "SuperMarioWorld" in env.gamename:
                    # print(_info["level"], _info["x_position"]//TILE_SIZE, _info["y_position"]//TILE_SIZE)
                    x_y_level.add((_info["level"], _info["x_position"]//TILE_SIZE, _info["y_position"]//TILE_SIZE))

                if "Zelda" in env.gamename:
                    x_y_level.add((_info["dungeonindex"],_info["worldindex"], _info["xPosLink"]//TILE_SIZE, _info["yPosLink"]//TILE_SIZE))

                steps += 1
                
                # Consider saving every step.
                if(steps % args.img_save_count == 0):
                    all_embeddings_images.append(get_embedding(obs))
                    filename = f"Screencap_{steps}.png"
                    imsave(os.path.join(result_dir_image_path, filename), obs)
                    all_possibilities_len.append(len(x_y_level))

                if(steps % 5000 == 0):
                    print(f"Total seconds since start: {time.time() - start_time:.1f}")


        explore_count.append(1)
        observations.append(obs)
        embedded_obs = get_embedding(obs)
        embeddings.append(embedded_obs)
        all_embeddings.append(embedded_obs)
        steps_per_iteration.append(steps)
        save_states.append(env.em.get_state())

        np.maximum(max_embedding_val, embedded_obs, out=max_embedding_val)
        np.minimum(min_embedding_val, embedded_obs, out=min_embedding_val)

    logging_dict = dict()
    logging_dict["steps"] = steps
    logging_dict["rrt_expansion"] = rrt_expansion
    logging_dict["state_switch_iteration_counts"] = state_switch_iteration_counts
    logging_dict["steps_per_iteration"] = steps_per_iteration
    logging_dict["all_embeddings"] = all_embeddings_images
    logging_dict["all_possibilities_len"] = all_possibilities_len
    return logging_dict

def mcts_play():
    raise NotImplementedError

# BREADCRUMBS_END
