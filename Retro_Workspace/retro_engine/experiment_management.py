import pickle, os, time
import numpy as np
from retro_engine.visualization import visualize_results
import shutil
import retro
import glob
import cv2


#####################
# GET THE SCORE.    #
#####################
def get_nn_score(embeddings):

    embeddings = np.array(embeddings)
    length = len(embeddings)
    bbox_sum = np.zeros(length-1)
    nuc_norm = np.zeros(length-1)
    for i in range(2, length):
        bbox_sum[i-1] = sum(np.amax(embeddings[:i], axis=0) - np.amin(embeddings[:i], axis=0))
        nuc_norm[i-1] = np.linalg.norm(np.cov(embeddings[:i].T), ord='nuc')

    return bbox_sum, nuc_norm

def calculate_score(logging_dict) -> dict:
    bbox_sum, nuc_norm = get_nn_score(logging_dict["all_embeddings"])

    logging_dict['bbox_sum'] = bbox_sum
    logging_dict['nuc_norm'] = nuc_norm
    return logging_dict


def make_gif(gif_name:str, dir_path):
    file_names = sorted(glob.glob(f"{dir_path}/Images/*"), key=os.path.getmtime)
   
    gif_buffer = np.array([cv2.imread(file) for file in file_names])
    
    height, width = gif_buffer.shape[1], gif_buffer.shape[2]
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    video = cv2.VideoWriter(f'{dir_path}/Movie/{gif_name}.mp4', fourcc, float(60), (width, height))
    for image in gif_buffer:
        video.write(image)
    video.release()
    print(f"\nMovie succesfully saved at {dir_path}/Movie/{gif_name}.mp4")

def make_gif_from_array(gif_name:str, dir_path, img_array):
    print("HERE")
    height, width = img_array.shape[1], img_array.shape[2]
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    video = cv2.VideoWriter(f'{dir_path}/Movie/{gif_name}.mp4', fourcc, float(60), (width, height))
    for image in img_array:
        video.write(image)
    video.release()
    print(f"\nMovie succesfully saved at {dir_path}/Movie/{gif_name}.mp4")

result_dir_path = ""
def human_experiment_create_folders(game_name:str, movie_name:str, testing:bool)->str:
    global result_dir_path
    data_dir_path = f"Data/{game_name}/{movie_name}"
    if not os.path.exists(data_dir_path):
        os.makedirs(data_dir_path)

    if not os.path.exists(data_dir_path + "/Movie"):
        os.makedirs(data_dir_path + "/Movie")

    if not os.path.exists(data_dir_path + "/Images"):
        os.makedirs(data_dir_path + "/Images")

    if not os.path.exists(data_dir_path + "/Ram"):
        os.makedirs(data_dir_path + "/Ram")

    result_dir_path = f"ExperimentResults/{game_name}/{movie_name}"
    if not os.path.exists(result_dir_path):
        os.makedirs(result_dir_path)
    elif testing:
        pass
    else:
        print('\033[91m' + f"Error: ExperimentResults/{game_name}/{movie_name} already exists. Please give a new movie name." + '\033[0m')
        exit()

    result_dir_path = f"ExperimentResults/{game_name}/{movie_name}/HumanPlay_incomplete"
    if not os.path.exists(result_dir_path):
        os.makedirs(result_dir_path)

    all_graphs_path = f"ExperimentResults/{game_name}/{movie_name}/All_Graphs"
    if not os.path.exists(all_graphs_path):
        os.makedirs(all_graphs_path)

    result_dir_image_path = result_dir_path + f"/Images"
    if not os.path.exists(result_dir_image_path):
        os.makedirs(result_dir_image_path)

    ram_dir = result_dir_path + f"/Ram"
    if not os.path.exists(ram_dir):
        os.makedirs(ram_dir)

    bk2_dir = result_dir_path + f"/Movie"
    if not os.path.exists(bk2_dir):
        os.makedirs(bk2_dir)

    return result_dir_path, data_dir_path


def experiment_setup(args)-> ("Env", "np.array", str, str):
    pickle_path = None
    plot_title = f"{args.game_name} "
    ###############################
    # SET THE DIRECTORY STRUCTURE #
    ###############################
    if (args.algorithm == "movie") and not bool(args.movie_name):
        print('\033[91m' + "Error: Movie algorithm without a movie. Please specify a movie by using -m tag and giving a movie name." +  '\033[0m')
        exit()

    result_dir_path = f"ExperimentResults/{args.game_name}"
    if not os.path.exists(result_dir_path):
        os.makedirs(result_dir_path)
    

    if args.movie_name:
        print(f"Loading from {args.movie_name}")
        
        result_dir_path = f"ExperimentResults/{args.game_name}/{args.movie_name}"
        if not os.path.exists(result_dir_path):
            os.makedirs(result_dir_path)
   

        all_graphs_path = result_dir_path + "/All_Graphs"
        if not os.path.exists(all_graphs_path):
            os.makedirs(all_graphs_path)

        experiment_number = len(glob.glob(f"{result_dir_path}/*"))
        plot_title += f"{args.movie_name} Experiment {experiment_number} {args.algorithm}"


        result_dir_path = result_dir_path + f"/experiment_{experiment_number}_{args.algorithm}_incomplete"
        if not os.path.exists(result_dir_path):
            os.makedirs(result_dir_path)

        bk2_dir = result_dir_path + f"/Movie"
        if not os.path.exists(bk2_dir):
            os.makedirs(bk2_dir)
            
        pickle_dir_path = f"Data/{args.game_name}/{args.movie_name}"
        if args.specific_pickle:
            pickle_path = f"{pickle_dir_path}/{args.movie_name}_{args.specific_pickle}.pickle"
        else:
            pickle_files = glob.glob(f"{pickle_dir_path}/*.pickle")
            if len(pickle_files) > 1 and args.algorithm != "movie":
                print('\033[91m' + "Error: More than one pickle file found. Please specify which one by using -p tag and giving the state count." +  '\033[0m')
                exit()
            pickle_path = glob.glob(f"{pickle_dir_path}/*.pickle")[0]

        with open(pickle_path, 'rb') as f:
            states = pickle.load(f)
            num_states = len(states)
            print(f"Loaded {num_states} from {pickle_path}")
    else:
        print("No movie found. Starting from game start.")
        result_dir_path = f"ExperimentResults/{args.game_name}/_StartFromTop"
        if not os.path.exists(result_dir_path):
            os.makedirs(result_dir_path)


        all_graphs_path = result_dir_path + "/All_Graphs"
        if not os.path.exists(all_graphs_path):
            os.makedirs(all_graphs_path)

        experiment_number = len(glob.glob(f"{result_dir_path}/*"))
        result_dir_path = result_dir_path + f"/experiment_{experiment_number}_{args.algorithm}_incomplete"
        if not os.path.exists(result_dir_path):
            os.makedirs(result_dir_path)

        bk2_dir = result_dir_path + f"/Movie"
        if not os.path.exists(bk2_dir):
            os.makedirs(bk2_dir)

        plot_title += f"_StartFromTop: experiment {experiment_number}"
        env = retro.make(args.game_name, use_restricted_actions=retro.Actions.ALL, state=None, record=bk2_dir + "/Movie")
        states = []

    result_dir_image_path = result_dir_path + f"/Images"
    if not os.path.exists(result_dir_image_path):
        os.makedirs(result_dir_image_path)

    return states, result_dir_path, plot_title


#####################
# LOG THE DATA.     #
#####################
def human_experiment_record_data(logging_dict, states, start_time):
    end_time = time.time()
    pickle_path = result_dir_path + "/log_data.pickle"

    with open(pickle_path, 'wb') as f:
        pickle.dump(logging_dict, f,  pickle.DEFAULT_PROTOCOL)

    print("\n######################## RESULTS ######################################")
    print("# ")
    print(f"# The final BBsum metric is {logging_dict['bbox_sum'][-1]:.2f}.")
    print(f"# The final Nuclear Norm metric {logging_dict['nuc_norm'][-1]:.2f}.")
    print("# ")
    print(f"# Experiment end time: {time.asctime(time.localtime(time.time()))}")
    print(f"# Total run time: {end_time - start_time}")
    print(f"# Total steps: {logging_dict['steps']}")
    print("########################################################################\n")

    with open(os.path.join(f"{result_dir_path}", "README.txt"), "w") as readme:
        print("\n########################################################################", file=readme)
        print(f"# The final BBsum metric is {logging_dict['bbox_sum'][-1]:.2f}.", file=readme)
        print(f"# The final Nuclear Norm metric {logging_dict['nuc_norm'][-1]:.2f}.", file=readme)
        print("# ", file=readme)
        print(f"# Experiment end time: {time.asctime(time.localtime(time.time()))}", file=readme)
        print(f"# Total run time: {end_time - start_time}", file=readme)
        print(f"# Total steps: {logging_dict['steps']}", file=readme)
        print("# ", file=readme)
        print("########################################################################\n", file=readme)


    data_dir_path = f"Data/{logging_dict['game_name']}/{logging_dict['movie_name']}"
    state_save_name = f"{logging_dict['movie_name']}_{len(states)}.pickle"
    pickle_path = os.path.join(data_dir_path, state_save_name)
    with open(pickle_path, 'wb') as f:
        pickle.dump(states, f,  pickle.HIGHEST_PROTOCOL)
    print(f"Succesfuly saved a total of {len(states)} states at {pickle_path}.")
    shutil.move(result_dir_path, result_dir_path[:-11])

