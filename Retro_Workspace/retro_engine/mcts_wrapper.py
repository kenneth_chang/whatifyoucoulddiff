from itertools import chain, combinations
import collections
import gym
import copy

class Action():
    def __init__(self, action):
        self.action = action

    def __eq__(self, other):
        return self.action == other.action

    def __hash__(self):
        return tuple(self.action)

class MCTS_Wrapper(gym.Wrapper):
    def get_all_action(self, size):
        length = len(f"{size:b}")
        all_actions = list()
        for number in range(size+1):
            action = tuple([int(binary) for binary in '{:0{}b}'.format(number, length)])
            all_actions.append(action)
        print(all_actions)
        return all_actions

    def __init__(self, env, max_episode_steps=None):
        super(MCTS_Wrapper, self).__init__(env)
        self.reward:int = 0
        self.done:bool = False
        action_space = env.action_space.high - env.action_space.low
        self.all_actions = self.get_all_action(action_space[0])
        
    def step(self, action):
        observation, reward, done, info = self.env.step(action)
        self.reward = reward
        self.done = done
        return observation, reward, done, info

    def reset(self):
        self.env.reset()


#    def getMctsState():
#        return new WrappedState(self)
# New class will hold a refernce to the wrapper, instead of being the wrapper.
######### implement Composition ################

    def getPossibleActions(self):
        return self.all_actions

    def takeAction(self, action):
        self.step(action)
        self.env.render()
        return copy.deepcopy(self)

    def isTerminal(self):
        return self.done

    def getReward(self):
        # only needed for terminal states
        return self.reward
