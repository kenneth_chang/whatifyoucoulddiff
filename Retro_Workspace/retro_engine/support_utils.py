import smtplib
import numpy as np
import time
import re
import os, glob


from os.path import basename


def save_hyperparameters(filenames: list, path_to_file: str,  experiment_name:str=None, save_prob=None, load_prob=None, breadcrumb="# BREADCRUMBS") -> None:
    """
    Saves the lines in between breadcrumbs in all the given filenames. This is used for saving hyperparameters for RL training.
    Parameters
    ----------
    breadcrumb: Writes the lines between {breadcrumb}_START and {breadcrumb}_END.
    """
    with open(path_to_file, "a") as dest:
        if save_prob:
            dest.write(f"Save prob:{save_prob} \n")
        if load_prob:
            dest.write(f"Load prob:{load_prob} \n")
        for filename in filenames:
            with open(filename, "r") as source:
                saving = False
                for line in source:
                    if line.strip() == f"{breadcrumb}_START":
                        dest.write("\n")
                        saving = True
                        continue
                    if line.strip() == f"{breadcrumb}_END":
                        saving = False
                        continue
                    if saving:
                        dest.write(line)
            print(f"{filename} hyperparameters have been saved!")
        print("Information saving is complete!")
