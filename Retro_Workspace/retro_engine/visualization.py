import matplotlib.pyplot as plt
import os

def visualize_results(log_data, title_name, result_dir_path):
    all_graphs_path = os.path.dirname(result_dir_path) + f"/All_Graphs"

    if log_data['algorithm'].lower() == "rrt":
        color = "blue"
    if log_data['algorithm'].lower() == "forest":
        color = "blue"
    if log_data['algorithm'].lower() == "human":
        color = "green" 
    if log_data['algorithm'].lower() == "movie":
        color = "green"    
    if log_data['algorithm'].lower() == "random":
        color = "red"
    if log_data['algorithm'].lower() == "chaos":
        color = "red"

    fig = plt.figure()
    ax = plt.axes()
    x_axis = range(len(log_data["bbox_sum"]))
    ax.plot( x_axis, log_data["bbox_sum"], color=color)
    if "state_switch_iteration_counts" in log_data.keys():
        xcoords = log_data["state_switch_iteration_counts"]
        for xc in xcoords:
            plt.axvline(x=xc, linestyle='--', color="orange", alpha=.45, label="State changes")
    plt.title(f"Bounding Box in {title_name}")
    # plt.savefig(f'{result_dir_path}/bbox_{title_name}.png')
    # plt.savefig(f'{all_graphs_path}/bbox_{title_name}.png')

    fig = plt.figure()
    ax = plt.axes()
    ax.plot( x_axis, log_data["nuc_norm"], color=color)
    if "state_switch_iteration_counts" in log_data.keys():
        xcoords = log_data["state_switch_iteration_counts"]
        for xc in xcoords:
            plt.axvline(x=xc, linestyle='--', color="orange", alpha=.45)
    plt.xlabel('RRT Expansions') 
    plt.ylabel('Exploration') 
    plt.title(f"Nuclear norm metric in {title_name}")
    # plt.savefig(f'{result_dir_path}/norm_{title_name}.png')
    # plt.savefig(f'{all_graphs_path}/norm_{title_name}.png')

    fig = plt.figure()
    ax = plt.axes()
    x_axis = range(len(log_data["all_possibilities_len"]))
    ax.plot(x_axis, log_data["all_possibilities_len"], color=color)
    if "state_switch_iteration_counts" in log_data.keys():
        xcoords = log_data["state_switch_iteration_counts"]
        for xc in xcoords:
            plt.axvline(x=xc, linestyle='--', color="orange", alpha=.45)
    plt.xlabel('100 frames ') 
    plt.ylabel('Touched tile counts') 
    plt.title(f"Total Touched Tiles in {title_name}")
    plt.savefig(f'{result_dir_path}/Total Touched Tiles {title_name}.png')
    plt.savefig(f'{all_graphs_path}/Total Touched Tiles {title_name}.png')
    
