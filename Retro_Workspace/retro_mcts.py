from retro_engine.mcts_wrapper import MCTS_Wrapper
import gym
import gym_mcts
from mcts import mcts

import random
random.seed(0)
env = gym.make("mcts-trivial-v0")
env = MCTS_Wrapper(env)
env.reset()

mcts = mcts(iterationLimit=2)
action = mcts.search(initialState=env)

print(action)