import subprocess as sp;
import datetime;
import os
import time

#constant variables
modified_roms_path = "/home/nikhil/whatifyoucoulddiff/modified_roms";
rom_location_path = "/home/nikhil/.local/lib/python3.6/site-packages/retro/data/stable/SuperMarioWorld-Snes";
directory =  "/home/nikhil/whatifyoucoulddiff/Retro_Workspace/human_playthrough.py"

#variables
now = datetime.datetime.now();
#first get all the user info -- prompt the user for it

#get id of person running tests
idProct = input("Your cruzid:\n");
#get tester's id name/name
idTester = input("Your tester's id/name:\n");
#get rom name -- must be in modified roms
rom = input("Name of the rom?\n");
rom += ".sfc"
#next get date
date = str(now.year) + "-" + str(now.month) + '-' + str(now.day);
date = now.strftime("%Y-%m-%d_%H:%M");
#next get time limit in minutes
timeLimit = input("Time Limit: (ints only)\n");


#frame count
frames = input("Number of Frames: (ints only)\n");
#check all the types of the correct type
#only thing we need to check here is if the timeLimit is an int
try:
    isinstance(int(frames),int);
except Exception as e:
    print("frames is not an integer\n")
    exit(1)

try:
    isinstance(int(timeLimit),int);
except Exception as e:
    print("timeLimit is not an integer\n")
    exit(1)

timeout = time.time() + 60*int(timeLimit)   # 5 minutes from now
#title of gameplay
title = idProct + "_" + idTester + "_" + rom + "_" + date + "_" + timeLimit

#check if the rom exists
exists = os.path.isfile(modified_roms_path + '/' + rom);

if exists == False:
    print(rom + " not found in " + modified_roms_path);
    exit(1)

rom_path = modified_roms_path + '/' + rom;

#now check if there is a rom in the rom_location_path
exists = os.path.isfile(rom_location_path + '/' + "rom.sfc")

if exists:
    #remove the file rom.sfc
    cmd = 'rm ' + rom_location_path + '/' + "rom.sfc"
    os.system(cmd)

#now copy the file into the directory
cmd = 'cp '+ modified_roms_path + '/' + rom + ' ' + rom_location_path + '/'
os.system(cmd);
#now rename this rom
cmd = 'mv ' + rom_location_path + '/' + rom + ' ' + rom_location_path + '/' + "rom.sfc"
os.system(cmd)

#now the file is in the directory proceed to run retro to get player gameplay
#run the retro command

#first check the script exists
exists = os.path.isfile(directory);

if exists == False:
    print("script not found inside directory");
    exit(1)

#now run the human_playthrough in the background
cmd = 'nohup python3 ' + directory + ' -m ' + title + ' -f ' + frames + ' &'
os.system(cmd)

#sleep to allow for program to launch
time.sleep(3)
#timer that is set in minutes as specified above
while True:
    test = 0
    if test == timeLimit or time.time() > timeout:
        break
    test = test - 1

#search for the human_playthrough pid
os.system('ps ax | grep human_playthrough.py > pid.txt');
#read the file and find the pid for the human_playthrough
with open('pid.txt','r') as f:
    pid = f.readlines()[0].split(' ')[1];
    cmd = 'kill ' + pid

#killing the human_playthrough
os.system(cmd)

#finally removing some extra files we don't need
cmd = 'rm pid.txt'
os.system(cmd)

#leaving nohup.out just in case something doesn't work
